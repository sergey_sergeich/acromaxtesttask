package me.sergeich.playerview;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import me.sergeich.downloader.DownloaderService;
import me.sergeich.player.MediaPlayerImpl;
import me.sergeich.player.Player;

public class PlayerView extends FrameLayout {

    private ImageButton imageButton;
    private ProgressBar progressBar;

    private State state = State.UNINITIALIZED;

    private Uri fileUri;

    private Player player;

    public enum State {
        UNINITIALIZED, FETCHING, PLAYING, PAUSED, COMPLETED
    }

    public PlayerView(Context context) {
        this(context, null);
    }

    public PlayerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PlayerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_player, this);

        imageButton = findViewById(R.id.view_player_button);
        progressBar = findViewById(R.id.view_player_progress);

        setState(State.UNINITIALIZED);

        imageButton.setOnClickListener(buttonClickListener);

        player = new MediaPlayerImpl(context);
        player.setOnCompletedListener(onCompletedListener);
    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {
        imageButton.setOnTouchListener(l);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        IntentFilter filter = new IntentFilter(DownloaderService.ACTION_DOWNLOADED);
        filter.addDataScheme("file");
        LocalBroadcastManager
                .getInstance(getContext())
                .registerReceiver(downloadBroadcastReceiver, filter);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        LocalBroadcastManager
                .getInstance(getContext())
                .unregisterReceiver(downloadBroadcastReceiver);
    }

    private void setState(State state) {
        this.state = state;
        updateState();
    }

    private void updateState() {
        switch (state) {
            case UNINITIALIZED:
                imageButton.setImageResource(android.R.drawable.ic_media_play);
                imageButton.setOnClickListener(buttonClickListener);
                progressBar.setVisibility(GONE);
                break;
            case FETCHING:
                imageButton.setImageResource(android.R.color.transparent);
                imageButton.setOnClickListener(null);
                progressBar.setVisibility(VISIBLE);
                break;
            case PLAYING:
                imageButton.setImageResource(android.R.drawable.ic_media_pause);
                imageButton.setOnClickListener(buttonClickListener);
                progressBar.setVisibility(GONE);
                break;
            case PAUSED:
                imageButton.setImageResource(android.R.drawable.ic_media_play);
                imageButton.setOnClickListener(buttonClickListener);
                progressBar.setVisibility(GONE);
                break;
            case COMPLETED:
                imageButton.setImageResource(android.R.color.transparent);
                imageButton.setOnClickListener(null);
                progressBar.setVisibility(VISIBLE);
                break;
        }
    }

    private BroadcastReceiver downloadBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (DownloaderService.ACTION_DOWNLOADED.equals(intent.getAction())) {
                setUri(intent.getData());
                play();
            }
        }
    };

    private void setUri(Uri uri) {
        this.fileUri = uri;
        player.setUri(uri);
    }

    private OnClickListener buttonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (state) {

                case UNINITIALIZED:
                    fetch();
                    break;
                case PLAYING:
                    pause();
                    break;
                case PAUSED:
                    play();
                    break;
                case FETCHING:
                case COMPLETED:
                    break;
            }
        }
    };

    private Player.OnCompletedListener onCompletedListener = new Player.OnCompletedListener() {
        @Override
        public void onCompleted() {
            setState(State.COMPLETED);
        }
    };

    private void fetch() {
        setState(State.FETCHING);

        Intent intent = new Intent(getContext(), DownloaderService.class);
        intent.putExtra(DownloaderService.ARGS.INDEX, "hls_index.m3u8");
        intent.putExtra(DownloaderService.ARGS.DOWNLOAD_TO, "output.ts");
        getContext().startService(intent);
    }

    private void pause() {
        setState(State.PAUSED);
        player.pause();
    }

    private void play() {
        setState(State.PLAYING);
        player.play();
    }
}
