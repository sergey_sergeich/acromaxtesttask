package me.sergeich.dragger;

import android.app.Activity;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.Window;

public class Dragger {
    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 200;

    private View view;

    private VelocityTracker velocityTracker;

    private float startX;
    private float startY;
    private float dX;
    private float dY;
    private int lastAction;

    public Dragger() {
    }

    public void setView(View view) {
        this.view = view;
        view.setOnTouchListener(onTouchListener);
    }

    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (velocityTracker == null) {
                velocityTracker = VelocityTracker.obtain();
            }
            velocityTracker.addMovement(event);

            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    startX = event.getRawX();
                    startY = event.getRawY();
                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    return false;

                case MotionEvent.ACTION_MOVE:
                    view.setX(event.getRawX() + dX);
                    view.setY(event.getRawY() + dY);
                    lastAction = MotionEvent.ACTION_MOVE;
                    return true;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN) {
                        return false;
                    } else if (lastAction == MotionEvent.ACTION_MOVE) {
                        velocityTracker.computeCurrentVelocity(1000);
                        int velocityX = (int) velocityTracker.getXVelocity();
                        int velocityY = (int) velocityTracker.getYVelocity();
                        int diffX = (int) (event.getRawX() - startX);
                        int diffY = (int) (event.getRawY() - startY);

                        if (Math.abs(diffX) > Math.abs(diffY)) {
                            if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                                if (diffX > 0) {
                                    onSwipeRight();
                                } else {
                                    onSwipeLeft();
                                }
                            }
                        } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffY > 0) {
                                onSwipeBottom();
                            } else {
                                onSwipeTop();
                            }
                        }

                        view.setPressed(false);
                        return true;
                    }

                default:
                    return false;
            }
        }
    };

    private void onSwipeTop() {
        view.setY(0);
    }

    private void onSwipeBottom() {
        int screenHeight = getScreenHeight();
        int viewHeight = view.getMeasuredHeight();
        view.setY(screenHeight - viewHeight);
    }

    private void onSwipeLeft() {
        view.setX(0);
    }

    private void onSwipeRight() {
        view.setX(getScreenWidth() - view.getMeasuredWidth());
    }

    private int getScreenHeight() {
        return ((Activity) view.getContext())
                .getWindow()
                .findViewById(Window.ID_ANDROID_CONTENT)
                .getHeight();
    }

    private int getScreenWidth() {
        return ((Activity) view.getContext())
                .getWindow()
                .findViewById(Window.ID_ANDROID_CONTENT)
                .getWidth();
    }
}
