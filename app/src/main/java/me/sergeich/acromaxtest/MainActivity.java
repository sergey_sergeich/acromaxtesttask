package me.sergeich.acromaxtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import me.sergeich.dragger.Dragger;
import me.sergeich.playerview.PlayerView;

public class MainActivity extends AppCompatActivity {

    private Dragger dragger = new Dragger();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PlayerView playerView = findViewById(R.id.main_player);
        dragger.setView(playerView);
    }
}
