package me.sergeich.downloader;

class Chunk {
    private String uri;
    private long offset;
    private long length;
    private byte[] bytes;

    Chunk(String uri, long offset, long length) {
        this.uri = uri;
        this.offset = offset;
        this.length = length;
    }

    public String getUri() {
        return uri;
    }

    public long getOffset() {
        return offset;
    }

    public long getLength() {
        return length;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public byte[] getBytes() {
        return bytes;
    }
}
