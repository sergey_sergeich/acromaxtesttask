package me.sergeich.downloader;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.iheartradio.m3u8.data.TrackData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

class ThreadedDownloader {
    private static final String LOG_TAG = "ThreadedDownloader";

    private final static int POOL_SIZE = 2;              // Thread pool size, 2 according to the task
    private final static int MAX_POOL_SIZE = POOL_SIZE;  // Max thread pool size
    private final static int KEEP_ALIVE_TIME = 5;

    private Context context;

    private LinkedBlockingQueue<Runnable> tasksQueue = new LinkedBlockingQueue<>();
    private ChunkStatuses chunkStatuses;
    private ThreadPoolExecutor threadPoolExecutor;

    private File resultFile;
    private OutputStream resultFileOutputStream;

    private final Api api;

    ThreadedDownloader(Context context, String resultFileName, String baseUri, List<TrackData> playlist) {
        this.context = context;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUri)
                .build();

        api = retrofit.create(Api.class);

        try {
            File resultFile = new File(context.getFilesDir() + File.separator + resultFileName);
            resultFile.createNewFile();
            this.resultFile = resultFile;
            resultFileOutputStream = new FileOutputStream(resultFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        threadPoolExecutor = new ThreadPoolExecutor(POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME, TimeUnit.SECONDS, tasksQueue);
        chunkStatuses = new ChunkStatuses();
        for (int i = 0; i < playlist.size(); i++) {
            TrackData trackData = playlist.get(i);
            Chunk chunk = new Chunk(
                    trackData.getUri(),
                    trackData.getByteRange().getOffset(),
                    trackData.getByteRange().getSubRangeLength());
            chunkStatuses.add(i, chunk);
        }
        Log.e(LOG_TAG, chunkStatuses.toString());
    }

    void start() {
        for (Map.Entry<Integer, Chunk> entry : chunkStatuses.getChunksMap().entrySet()) {
            int position = entry.getKey();
            Chunk chunk = entry.getValue();
            threadPoolExecutor.execute(new FetchOneChunkTask(position, chunk));
        }
    }

    private void sendResult() {
        Intent intent = new Intent(DownloaderService.ACTION_DOWNLOADED);
        intent.setData(Uri.fromFile(resultFile));
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private class FetchOneChunkTask implements Runnable {
        private int position;
        private Chunk chunk;

        FetchOneChunkTask(int position, Chunk chunk) {
            this.position = position;
            this.chunk = chunk;
        }

        @Override
        public void run() {
            try {
                Chunk downloadedChunk = downloadChunk(chunk);
                Log.e(LOG_TAG, position + " downloaded");
                chunkStatuses.setChunk(position, downloadedChunk);
                Log.e(LOG_TAG, chunkStatuses.toString());
                writeDownloadedChunks();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (chunkStatuses.isEmpty()) {
                Log.e(LOG_TAG, "Finished");
                threadPoolExecutor.shutdown();
                try {
                    resultFileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                sendResult();
            }
        }

        private Chunk downloadChunk(Chunk chunk) throws IOException {
            long startByte = chunk.getOffset();
            long endByte = chunk.getOffset() + chunk.getLength() - 1;
            @SuppressLint("DefaultLocale")
            String bytesRange = String.format("bytes=%d-%d", startByte, endByte);
            Call<ResponseBody> responseBodyCall = api.download(chunk.getUri(), bytesRange);
            Response<ResponseBody> response = responseBodyCall.execute();
            ResponseBody body = response.body();
            chunk.setBytes(body.bytes());
            return chunk;
        }
    }

    private synchronized void writeDownloadedChunks() throws IOException {
        Log.e(LOG_TAG, "writeDownloadedChunks()");
        while (chunkStatuses.hasDownloadedAtHead()) {
            Chunk chunk = chunkStatuses.popFirstDownloaded();
            resultFileOutputStream.write(chunk.getBytes());
        }
        Log.e(LOG_TAG, chunkStatuses.toString());
    }


    private static class ChunkStatus {
        private Chunk chunk;
        private boolean downloaded = false;

        ChunkStatus(Chunk chunk) {
            this.chunk = chunk;
        }

        ChunkStatus(Chunk chunk, boolean downloaded) {
            this.chunk = chunk;
            this.downloaded = downloaded;
        }
    }

    private static class ChunkStatuses {
        private ConcurrentSkipListMap<Integer, ChunkStatus> map = new ConcurrentSkipListMap<>();

        void add(int position, Chunk chunk) {
            map.put(position, new ChunkStatus(chunk));
        }

        void setChunk(int position, Chunk chunk) {
            map.put(position, new ChunkStatus(chunk, true));
        }

        Map<Integer, Chunk> getChunksMap() {
            Map<Integer, Chunk> chunksMap = new HashMap<>();
            for (Map.Entry<Integer, ChunkStatus> entry : map.entrySet()) {
                chunksMap.put(entry.getKey(), entry.getValue().chunk);
            }
            return chunksMap;
        }

        boolean hasDownloadedAtHead() {
            return !map.isEmpty() && map.get(map.firstKey()).downloaded;
        }

        Chunk popFirstDownloaded() {
            if (hasDownloadedAtHead()) {
                Map.Entry<Integer, ChunkStatus> entry = map.pollFirstEntry();
                return entry.getValue().chunk;
            }
            return null;
        }

        boolean isEmpty() {
            return map.isEmpty();
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            for (int id : map.keySet()) {
                sb.append("<").append(id).append(",").append(map.get(id).downloaded).append(">,");
            }
            sb.append("]");
            return sb.toString();
        }
    }
}
