package me.sergeich.downloader;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.iheartradio.m3u8.Encoding;
import com.iheartradio.m3u8.Format;
import com.iheartradio.m3u8.PlaylistParser;
import com.iheartradio.m3u8.data.MediaData;
import com.iheartradio.m3u8.data.MediaType;
import com.iheartradio.m3u8.data.Playlist;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * Downloads index playlist, selects the best audiotrack, dowloads it and writes to file
 * Returns URI of saved file in LocalBroadcast with action @see {@link DownloaderService#ACTION_DOWNLOADED}
 */
public class DownloaderService extends IntentService {

    private final static String API_BASE = "http://pubcache1.arkiva.de/test/";

    public static class ARGS {
        public static final String INDEX = "me.sergeich.downloader.DownloaderService.ARGS.INDEX";
        public static final String DOWNLOAD_TO = "me.sergeich.downloader.DownloaderService.ARGS.DOWNLOAD_TO";
    }

    public static final String ACTION_DOWNLOADED = "me.sergeich.downloader.DownloaderService.ACTION_DOWNLOADED";

    private Api api;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public DownloaderService() {
        super("DownloaderService");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE)
                .build();

        api = retrofit.create(Api.class);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String indexUri = intent.getStringExtra(ARGS.INDEX);
        String resultFileName = intent.getStringExtra(ARGS.DOWNLOAD_TO);

        try {
            InputStream indexInputStream = requestFile(indexUri);
            Playlist indexPlaylist = parsePlaylist(indexInputStream);
            List<MediaData> media = indexPlaylist.getMasterPlaylist().getMediaData();
            List<MediaData> audio = filterAudio(media);
            MediaData hqAudio = selectBestTrack(audio);

            InputStream audioIS = requestFile(hqAudio.getUri());
            Playlist audioPlaylist = parsePlaylist(audioIS);

            ThreadedDownloader downloader =
                    new ThreadedDownloader(getBaseContext(), resultFileName, API_BASE, audioPlaylist.getMediaPlaylist().getTracks());
            downloader.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private InputStream requestFile(String uri) throws IOException {
        Call<ResponseBody> download = api.download(uri);
        return download.execute().body().byteStream();
    }

    private Playlist parsePlaylist(InputStream is) throws IOException {
        InputStream inputStream = new BufferedInputStream(is);
        PlaylistParser parser = new PlaylistParser(inputStream, Format.EXT_M3U, Encoding.UTF_8);
        Playlist playlist = null;
        try {
            playlist = parser.parse();
        } catch (Exception e) {
            // rethrow all exceptions from parser as IOException
            throw new IOException(e);
        }
        return playlist;
    }

    private List<MediaData> filterAudio(List<MediaData> media) {
        List<MediaData> audio = new ArrayList<>();
        for (MediaData mediaData : media) {
            if (mediaData.getType() == MediaType.AUDIO) {
                audio.add(mediaData);
            }
        }
        return audio;
    }

    private MediaData selectBestTrack(List<MediaData> audio) {
        int hq = 0;
        MediaData hqAudio = null;
        for (MediaData audioData : audio) {
            int quality = getQuality(audioData.getUri());
            if (quality > hq) {
                hqAudio = audioData;
            }
        }
        return hqAudio;
    }

    /**
     * Gets quality from filename of audio track. Expects filenames starting with "hls_a"
     *
     * @param name filename
     * @return quality of audio track
     */
    private int getQuality(String name) {
        try {
            Pattern p = Pattern.compile("hls_a(.*)K.*");
            Matcher m = p.matcher(name);
            if (m.find()) {
                String quality = m.group(1);
                return Integer.valueOf(quality);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

}
