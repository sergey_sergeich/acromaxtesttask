package me.sergeich.player;

import android.net.Uri;

public interface Player {

    void setUri(Uri uri);

    void play();

    void pause();

    void setOnCompletedListener(OnCompletedListener onCompletedListener);

    interface OnCompletedListener {
        void onCompleted();
    }
}
