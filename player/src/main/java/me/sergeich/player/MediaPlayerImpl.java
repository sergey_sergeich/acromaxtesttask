package me.sergeich.player;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;

public class MediaPlayerImpl implements Player {
    private Context context;
    private MediaPlayer mediaPlayer;
    private Uri fileUri;
    private OnCompletedListener onCompletedListener;

    public MediaPlayerImpl(Context context) {
        this.context = context;
    }

    @Override
    public void setUri(Uri uri) {
        this.fileUri = uri;
    }

    @Override
    public void play() {
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(context, fileUri);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnCompletionListener(onCompletionListener);
        }
        mediaPlayer.start();
    }

    @Override
    public void pause() {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
        }

    }

    @Override
    public void setOnCompletedListener(OnCompletedListener onCompletedListener) {
        this.onCompletedListener = onCompletedListener;
    }

    private MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            mediaPlayer.release();
            mediaPlayer = null;
            if (onCompletedListener != null) {
                onCompletedListener.onCompleted();
            }
        }
    };
}
